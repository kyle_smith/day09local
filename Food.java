public class Food
{
    private String description;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    /**
    * Set the description
    *@param inDescription the new description
    */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    @Oveerride
    public String toString()
    {
        return "Somebody brought " + getDescription();
    }    
    public int getCalories()
    {
    return calories;
    }
    public void setCalories(in.calories)
    {
    calories = in.calories;
    }
}